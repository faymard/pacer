import sys
import ac
import acsys

data_lasttime = 0
data_newtime = 0

data_pace = 0.0

label_pace = None

def acMain(ac_version):
	global data_lasttime, data_newtime, label_pace, data_pace

	appWindow = ac.newApp("pACer")
	ac.setSize(appWindow, 200, 100)

	label_pace = ac.addLabel(appWindow, data_pace)

	ac.setCustomFont(label_pace, "Arial", 0,1)
	ac.setFontAlignment(label_pace, "center")
	ac.setFontSize(label_pace,28)
	ac.addRenderCallback(appWindow, onFormRender)
	return "pACer"

def onFormRender(deltaT):


	global data_lasttime, data_newtime, label_pace, data_pace
	data_newtime = ac.getCarState(0, acsys.CS.LastLap)
	if data_lasttime != data_newtime and data_lasttime != 0:
		data_pace = (data_newtime - data_lasttime)/1000.0
		ac.setText(label_pace, ('+' if data_pace >= 0 else '-') + "{:.3f}".format(abs(data_pace)))

		if data_pace <= 0:
			ac.setFontColor(label_pace, 0,1,0,1)
		else:

			ac.setFontColor(label_pace, 1,0,0,1)
	ac.setPosition(label_pace, 100.0, 50.0)
	data_lasttime = data_newtime

